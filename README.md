# Reddit Save Exporter

## What is it

Smol python script to export your saved posts if you're like me and have way too many and want to organize them.

## Set up
Create an app on your reddit account to get a secret and ID. See this article for steps - https://progur.com/2016/09/how-to-create-reddit-bot-using-praw4.html/

Once you have a secret and ID, paste those along with your username/pass into config.json.

## Use
Python 3 required.

```
usage: main.py [-h] [-d] [-s]

optional arguments:
  -h, --help      show this help message and exit
  -d, --download  Download Saved Posts
  -s, --serve     Generates an HTML page to view markdown and serves it
```

```--download``` will fetch saved posts and create a markdown file containg every saved post organized by subreddit, along with the title, permalink, url if the saved post was a link.

In case you don't have a markdown viewer, ```--serve``` will take that markdown file, and create an html file to view the markdown from index.html.template. It will also start a simple web server to easily view it. It uses showdown to render the markdown.

Example output
```
» python3 main.py -d -s
Fetching saved posts
Fetching next list
Fetching next list
Fetching next list
Generating markdown
Creating html viewer
Serving markdown viewer at 8080 8080
