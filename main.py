import requests
import collections
import json
import os
import sys

try:
  import http.server
  import socketserver
except ImportError:
  print("Error importing http.server, python3 required")
  sys.exit()
import argparse

class Post():
  def __init__(self, link, permalink, title):
    self.link = link
    self.title = title
    self.permalink = permalink

  def formatMD(self):
    out = []
    # out.append("{} \n".format(self.title))
    # out.append("- {} \n".format(self.link))
    out.append("<a href=https://www.reddit.com{}>{}</a>\n\n".format(self.permalink, self.title))
    return "".join(out)

def loadConfig(location):
  """"
  Reads in the configuration json
  """
  if not os.path.exists(location):
    print("File {} doesn't exist".format(location))
    sys.exit()

  with open(location) as f:
      config = json.load(f)

  return config

def loadCredentials(location):
  """
  Pulls credentials from our config
  """
  config = loadConfig(location)
  try:
    username = config['Credentials']['Username']
    password = config['Credentials']['Password']
    secret   = config['Credentials']['Secret']
    ID       = config['Credentials']['ID']
  except KeyError:
    print("Credentials not filled out")
    sys.exit()

  return username, password, secret, ID

def loadParameters(location):
  """
  Pulls non credential options from our config file
  """
  config = loadConfig(location)
  try:
    filename = config['Program']['Filename']
    port = config['Program']['Port']
  except KeyError:
    print("Parameters not filled out")
    sys.exit()

  return filename, port

def getToken(username, password, secret, ID):
  """
  Returns an auth token for a reddit user
  """
  client_auth = requests.auth.HTTPBasicAuth(ID, secret)
  post_data = {
                "grant_type": "password",
                "username":   username,
                "password":   password
              }
  headers = {
              "User-Agent": "exporter/0.1 /u/Octavaruim"
            }
  response = requests.post("https://www.reddit.com/api/v1/access_token", auth=client_auth, data=post_data, headers=headers)
  if response.ok:
    response = response.json()
    if 'access_token' not in response:
      return None, response
    return(response["access_token"]), None
  else:
    return None, response.text

def getPosts(token, postOffice, username, after=None):
  """
  Recursive function to get all saved posts from an account
  """
  headers = {}
  headers["User-Agent"] = "exporter/0.1 /u/Octavaruim"
  headers["Authorization"] = "Bearer {}".format(token)
  if after:
    print("Fetching next list")
    response = requests.get("https://oauth.reddit.com/user/{}/saved?show=all&limit=100&after={}".format(username, after), headers=headers)
  else:
    print("Fetching saved posts")
    response = requests.get("https://oauth.reddit.com/user/{}/saved?show=all&limit=100".format(username), headers=headers)
  if not response.ok:
    print("Error getting response: {}".format(response))
    return
  response = response.json()
  processPosts(response, postOffice)
  if response['data']['after'] != None:
    after = response['data']['after']
    getPosts(token, postOffice, username, after)
  else:
    return postOffice
  return postOffice

def processPosts(posts, postOffice):
  """
  Processes reddits api response and creates posts objects for each inside postOffice
  """
  for x in posts['data']['children']:
    subreddit = x['data']['subreddit'].lower().capitalize()
    if 'url' in x['data']:
      permalink = x['data']['permalink']
      title = x['data']['title']
      link = x['data']['url']
    else:
      permalink = link = x['data']['link_permalink']
      title = "Comment"
    post = Post(link, permalink, title)
    postOffice[subreddit].append(post)
  return postOffice

def makeMarkdown(postOffice, mfile):
  """
  Creates a markdown document from saved postOffice
  """
  print("Generating markdown")
  subreddits = list(postOffice.keys())
  subreddits.sort()
  with open(mfile,'w') as f:
    for subreddit in subreddits:
      f.write("### {} \n".format(subreddit))
      for post in postOffice[subreddit]:
        f.write(post.formatMD())
  return

def launchServer(port=8001):
  """
  Launches a simple http server in the current directory
  """
  handler = http.server.SimpleHTTPRequestHandler
  socketserver.TCPServer.allow_reuse_address=True
  httpd  = socketserver.TCPServer(("", port), handler)
  print("Serving markdown viewer at {}".format(port), port)
  try:
    httpd.serve_forever()
  except KeyboardInterrupt:
    print("Closing server")

def makeIndex(mfile, template):
  """
  Creates an html file to view a markdown file from our template
  """
  print("Creating html viewer")
  with open(template, "r") as f:
    text = f.readlines()
  with open(mfile, "r") as f:
    # Get line number where we want to insert our markdown
    index = [i for i, x in enumerate(text) if 'id="txt"' in x][0]
    line = text[index]
    new_line = []
    new_line.append(line)
    for line in f:
      new_line.append(line)
    new_line = "".join(new_line)
    text[index] = new_line
  with open("index.html", "w") as f:
    f.writelines(text)

def main():
  parser = argparse.ArgumentParser()
  parser = argparse.ArgumentParser()
  parser.add_argument("-d", "--download",
          help="Download Saved Posts", action="store_true")
  parser.add_argument("-s", "--serve",
          help="Generates an HTML page to view markdown and serves it",
          action="store_true")
  parser.parse_args()
  results = vars(parser.parse_args())


  if len(sys.argv) == 1:
    parser.print_help()
    sys.exit()

  filename, port, = loadParameters("config.json")

  if results['download']:
    username, password, secret, ID = loadCredentials("config.json")
    token, err = getToken(username, password, secret, ID)
    if not token:
      print("Could not get access token, check credentials: {}".format(err))
      return
    postOffice = collections.defaultdict(list)
    getPosts(token, postOffice, username)
    if not postOffice:
      print("No saved posts")
      return
    makeMarkdown(postOffice, filename)
  if results["serve"]:
    makeIndex(filename, "index.html.template")
    launchServer(port)

if __name__ == "__main__":
  main()

